//
//  BinaryDecimal.swift
//  binary-app
//
//  Created by Nadila Dithmal on 7/18/18.
//  Copyright © 2018 Nadila Dithmal. All rights reserved.
//

import Foundation

class BinaryDecimal {
    var bits: [Int]? //Properties for the class, variables to hold the values passed in
    var integer: Int?
    
    init(_ bits: [Int]) { //Initializer for Binary values
        self.bits = bits
    }
    
    init(_ decimal: Int) { //Initializer for Decimal values
        self.integer = decimal
    }
    
    func calculateBinaryValueForInt() -> String { //function to calculate Binary value of a given Integer
        let rows = [128, 64, 32, 16, 8, 4, 2, 1] //weight factors for a byte
        var binaryRows: [Int] = [] //
        
        var newInt = integer! //unwrap the optional 
        
        for row in rows { //loop through the byte array
            let binaryDigit = oneOrZero(forValue: newInt, withBitValue: row) //get one or zero form function
            binaryRows.append(binaryDigit) //append that byte to the array
            
            if binaryDigit == 1 {
                newInt = newInt - row //subtract the binary weight value form the int
            }
            
        }
        let stringFromIntArray = binaryRows.map { String($0) } //map and cast elements of the array as Strings
        return stringFromIntArray.joined() //'join' the elements of the array into one string and return
    }
    
    func calculateIntValueForBinary() -> String { //function to convert decimal to binary
        var value = 0 //properties
        var multiplier = 1
        
        guard let bits = bits else { return "Error" } //safely unwrapping the optional
        
        for bit in bits.reversed() { //cycling through the array reverse
            //let newValue = bit * multiplier
            value = value + bit * multiplier //adding multiplied value
            multiplier = multiplier * 2 //incrementing multiplier
        }
        
        return String(value)
    }
    
    func oneOrZero(forValue value: Int, withBitValue bitValue: Int) -> Int{ //function to determine zero or one
        if value - bitValue >= 0 { //if value subtracted by bit value is greater than zero
            return 1
        } else {
            return 0
        }
    }
    
}
